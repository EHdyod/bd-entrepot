import psycopg2
import pandas as pd
import os
from Manipulateur import Manipulateur 

if __name__ == "__main__":
    directory = os.path.dirname(os.path.abspath(__file__))
    db_infos= {"host":"localhost", "db":"entrepot", "user":"root", "pwd":"root"}

    # carac :   id, numacc,         lum,    agg,    int,    atm,    col,    com,    lat,        long,       dep,    date
    # ex :      0,  201700000001,   5,      2,      1,      1,      1,      477,    5051326.0,  292191.0,   590,    2017-01-11T18:20
    desc_carac = [
        ('id','int','PRIMARY KEY'),
        ('numacc','bigint'),
        ('lum','int', "REFERENCES lumiere"),
        ('agg','int', 'REFERENCES agglo'),
        ('int','int', 'REFERENCES intersection'),
        ('atm','int', 'REFERENCES atmosphere'),
        ('col','int', 'REFERENCES collision'),
        ('com','int'),
        ('lat','int'),
        ('long','int'),
        ('dep','int'),
        ('date','date')
    ]

    #lieux :    id, numacc,         catr,   surf
    # ex :      0,  201600000001,   3,      1
    desc_lieux = [
        ('id','int','PRIMARY KEY'),
        ('numacc','bigint'),
        ('catr','int', 'REFERENCES cat_route'),
        ('surf','int', 'REFERENCES surface')
    ]

    #usagers :  id, numacc,         catu,   grav,   sexe,   anneeNaissance, numveh
    # ex :      0,  201600000001,   1,      1,      2,      1983,           B02
    desc_usagers = [
        ('id','int','PRIMARY KEY'),
        ('numacc','bigint'),
        ('catu','int', 'REFERENCES cat_user'),
        ('grav','int', 'REFERENCES gravite'),
        ('sexe','int', 'REFERENCES sexe'),
        ('annee_naissance','int', 'DEFAULT NULL'),
        ('num_veh','text')
    ]
    
    #vehicules :    id, numacc,         catv,   obsm,   choc,   numveh
    #ex :           0,  201700000001,   7,      2,      3,      B01
    desc_vehicules = [
        ('id','int','PRIMARY KEY'),
        ('numacc','bigint'),
        ('catv','int', 'REFERENCES cat_veh'),
        ('obsm','int', 'REFERENCES obsm'),
        ('choc', 'int', 'REFERENCES choc'),
        ('num_veh', 'text')
    ]

    all_desc = {
        "caracteristiques":desc_carac, 
        "lieux":desc_lieux, 
        "usagers":desc_usagers, 
        "vehicules":desc_vehicules
    }

    table_names=["caracteristiques", "usagers", "lieux", "vehicules"]

    with Manipulateur(db_infos) as imp:
        for tab in table_names:
            file_path = f"{directory}/data/{tab}.csv"
            desc = all_desc[tab]
            df = pd.read_csv(file_path, sep=',')
            imp.create_table(tab, desc)
            for row in df.iterrows():
                index = row[0]
                datas = row[1]
                values = tuple(map(lambda a: a[1], datas.iteritems()))
                imp.insert(tab, values)
            
        imp.save()
