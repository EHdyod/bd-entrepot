import psycopg2
import os
from Manipulateur import Manipulateur 

if __name__ == "__main__":
    db_infos= {"host":"localhost", "db":"entrepot", "user":"root", "pwd":"root"}

    #Idee 1
    #Trouver la part de femme conductrice impliqué dans des accidents corporels, comparer ce résultat à celle de la part des hommes en fonction des régions
    vue1_select = {
        "Nombre":"count(sexe.description)",
        "Departement":"caracteristiques.dep"
    }
    vue1_from = ["usagers","caracteristiques","sexe","gravite","cat_user"]
    vue1_group = [*vue1_select][1]
    vue1_order = [*vue1_select][0]

    vue1F_name = "idee1_F"
    vue1F_where = {
        "sexe.description": ["Femme", True],
        "usagers.sexe" : ["sexe.id",True],
        "usagers.numacc": ["caracteristiques.numacc", True],
        "usagers.grav": ["gravite.id", True],
        "gravite.description" : ["Indemne", False]
    }
    vue1F = {
        'name': vue1F_name,
        'select': vue1_select,
        'from': vue1_from,
        'where': vue1F_where,
        'group': vue1_group,
        'order': vue1_order
    }

    vue1M_name = "idee1_M"
    vue1M_where = {
        "sexe.description": ["Homme", True],
        "usagers.sexe" : ["sexe.id",True],
        "usagers.numacc": ["caracteristiques.numacc", True],
        "usagers.grav": ["gravite.id", True],
        "gravite.description" : ["Indemne", False]
    }
    vue1M = {
        'name': vue1M_name,
        'select': vue1_select,
        'from': vue1_from,
        'where': vue1M_where,
        'group': vue1_group,
        'order': vue1_order
    }

    vue1_C_name = "idee1_summary"
    vue1_C_request = f"""
        CREATE VIEW {vue1_C_name} AS
        SELECT D.departement AS departement,
        CASE    WHEN (F.nombre - M.nombre)<0 THEN 'M'
                WHEN (F.nombre - M.nombre)>0 THEN 'F'
                ELSE 'EQUAL'
                END "Superior",
                F.nombre AS nombre_F,
                M.nombre AS nombre_M
        FROM "idee1_F" AS F, "idee1_M" AS M, "departements" AS D
        WHERE M.departement = F.departement AND F.departement = D.id;
    """
    views = [vue1F,vue1M]
    with Manipulateur(db_infos) as man:
        for view in views:
            man.create_view(view["name"], view["select"], view["from"], view["where"], view["group"], view["order"])
        man.create_view_raw(vue1_C_name, vue1_C_request)