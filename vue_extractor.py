import psycopg2
import os
from Manipulateur import Manipulateur 

if __name__ == "__main__":
    db_infos= {"host":"localhost", "db":"entrepot", "user":"root", "pwd":"root"}

    vue_names = [ "idee1_summary" ]
    with Manipulateur(db_infos) as man:
        for vue in vue_names:
            res = man.get_table_as_json(vue)
            print(res)
