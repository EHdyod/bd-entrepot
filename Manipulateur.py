import psycopg2
import pandas as pd
import os
import json

class Manipulateur():
    def __init__(self, infos):
        self.host = infos["host"]
        self.db = infos["db"]
        self.user = infos["user"]
        self.pwd = infos["pwd"]
        self.conn = None
        self.cur = None
    
    def __enter__(self):
        connString = f"host={self.host} dbname={self.db} user={self.user} password={self.pwd}"
        self.conn = psycopg2.connect(connString)
        self.cur = self.conn.cursor()
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.conn.close()
        self.cur = None
    
    def __exec__(self, req):
        res= None
        try:
            self.cur.execute(req)
        except Exception as ex:
            print(f"error in execution : {ex}")
        return res

    def save(self):
        try:
            self.conn.commit()
        except Exception as ex:
            print(ex)

    def doTableExiste(self, name):
        req = f"SELECT EXISTS(SELECT 1 FROM information_schema.tables WHERE table_catalog='{self.db}' AND table_schema='public' AND table_name='{name}');"

        self.cur.execute(req)
        res = self.cur.fetchone()
        return res[0]

    def drop_table(self, name):
        self.__exec__(f"DROP TABLE {name};")

    def create_table(self, name, desc):
        req = f"CREATE TABLE {name}( \n"

        for index, val in enumerate(desc):
            dname = val[0]
            dtype = val[1]
            doption = val[2] if len(val) == 3 else None
            req += f"{dname} {dtype} {doption if doption else ''} {',' if index != len(desc)-1 else '' }\n"

        req += ");"
        
        if self.doTableExiste(name):
            self.drop_table(name)
        self.__exec__(req)
        self.save()
      
    def insert(self, table, values):
        self.__exec__(f"INSERT INTO {table} values{values}")

    def select(self, name):
        res= None
        try:
            req = f'SELECT * FROM "{name}";'
            self.cur.execute(req)
            res = self.cur.fetchall()
        except Exception as ex:
            print(f"error in execution : {ex}")
        return res

    def drop_view(self, name):
        self.__exec__(f"DROP VIEW {name};")

    def create_view(self, name, select, fromTable, where, group = None, order = None):
        #select is dict with key as alias and value as real value
        #from is list of table used
        #where is dict with key is table's column and value is an array where index 0 is value and 1 is boolean where true is equal and false is different
        #group is an alias from select to group values
        #order is an alias from select to sort values

        req = f'CREATE VIEW "{name}" AS \n'
        req += f"SELECT"
        for key, value in select.items():
            req += f" {value} AS {key},"
        else:
            req = req[:-1]
            req += ' \n'
        
        req += "FROM"
        for val in fromTable:
            req += f" {val},"
        else:
            req = req[:-1]
            req += ' \n'
        
        req += "WHERE ("
        for i, (key, value) in enumerate(where.items()):
            val = f"'{value[0]}'" if value[0].find('.')==-1 else value[0]
            req += f"{'AND' if i != 0 else ''} {key} {'=' if value[1] else '<>'} {val}\n"
        else:
            req += ")\n"

        if group is not None: req += f"GROUP BY {group}\n" 
        if order is not None: req += f"ORDER BY {order}\n"
        req += ";"

        self.create_view_raw(name, req)

    def create_view_raw(self,name, request):
        if self.doTableExiste(name):
            self.drop_view(name)
        self.__exec__(request)
        self.save()

    def get_table_as_json(self, name):
        self.cur.execute(f'select json_agg(to_json(d)) from "{name}" AS d;')
        values = self.cur.fetchall()[0][0]
        with open(f"{name}.json", 'w') as f:
            json.dump(values, f)
        return os.path.exists(f"{name}.json")