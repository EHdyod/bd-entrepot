-- Adminer 4.7.7 PostgreSQL dump

DROP TABLE IF EXISTS "agglo";
CREATE TABLE "public"."agglo" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Agglo_id_agglo" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "agglo" ("id", "description") VALUES
(1,	'Hors agglomération                                                                                                                                                                                                                                             '),
(2,	'En agglomération                                                                                                                                                                                                                                               ');

DROP TABLE IF EXISTS "atmosphere";
CREATE TABLE "public"."atmosphere" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Atmosphere_id_atmos" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "atmosphere" ("id", "description") VALUES
(-1,	'Non renseigné                                                                                                                                                                                                                                                  '),
(1,	'Normale                                                                                                                                                                                                                                                        '),
(2,	'Pluie légère                                                                                                                                                                                                                                                   '),
(3,	'Pluie forte                                                                                                                                                                                                                                                    '),
(4,	'Neige - grêle                                                                                                                                                                                                                                                  '),
(5,	'Brouillard - fumée                                                                                                                                                                                                                                             '),
(6,	'Vent fort - tempête                                                                                                                                                                                                                                            '),
(7,	'Temps éblouissant                                                                                                                                                                                                                                              '),
(8,	'Temps couvert                                                                                                                                                                                                                                                  '),
(9,	'Autre                                                                                                                                                                                                                                                          ');

DROP TABLE IF EXISTS "cat_route";
CREATE TABLE "public"."cat_route" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "CatRoute_id_catr" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "cat_route" ("id", "description") VALUES
(1,	'Autoroute                                                                                                                                                                                                                                                      '),
(2,	'Route nationale                                                                                                                                                                                                                                                '),
(3,	'Route départementale                                                                                                                                                                                                                                           '),
(4,	'Voie communales                                                                                                                                                                                                                                                '),
(5,	'Hors réseau public                                                                                                                                                                                                                                             '),
(6,	'Parc de stationnement ouvert à la circulation publique                                                                                                                                                                                                         '),
(7,	'Route de métropole urbaine                                                                                                                                                                                                                                     '),
(9,	'Autre                                                                                                                                                                                                                                                          ');

DROP TABLE IF EXISTS "cat_user";
CREATE TABLE "public"."cat_user" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "CatUser_id_catu" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "cat_user" ("id", "description") VALUES
(1,	'Conducteur                                                                                                                                                                                                                                                     '),
(2,	'Passager                                                                                                                                                                                                                                                       '),
(3,	'Piéton                                                                                                                                                                                                                                                         '),
(4,	'Autre                                                                                                                                                                                                                                                          ');

DROP TABLE IF EXISTS "cat_veh";
CREATE TABLE "public"."cat_veh" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "CatVech_id_catVech" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "cat_veh" ("id", "description") VALUES
(0,	'Indeterminable                                                                                                                                                                                                                                                 '),
(1,	'Bicyclette                                                                                                                                                                                                                                                     '),
(2,	'Cyclomoteur <50cm3                                                                                                                                                                                                                                             '),
(3,	'Voiturette (Quadricycle à moteur carrossé) (anciennement "voiturette ou tricycle à moteur")                                                                                                                                                                    '),
(7,	'VL seul                                                                                                                                                                                                                                                        '),
(10,	'VU seul 1,5T <= PTAC <= 3,5T avec ou sans remorque (anciennement VU seul 1,5T <= PTAC <= 3,5T)                                                                                                                                                                 '),
(13,	'PL seul 3,5T <PTCA <= 7,5T                                                                                                                                                                                                                                     '),
(14,	'PL seul > 7,5T                                                                                                                                                                                                                                                 '),
(15,	'PL > 3,5T + remorque                                                                                                                                                                                                                                           '),
(16,	'Tracteur routier seul                                                                                                                                                                                                                                          '),
(17,	'Tracteur routier + semi-remorque                                                                                                                                                                                                                               '),
(20,	'Engin spécial                                                                                                                                                                                                                                                  '),
(21,	'Tracteur agricole                                                                                                                                                                                                                                              '),
(30,	'Scooter < 50 cm3                                                                                                                                                                                                                                               '),
(31,	'Motocyclette > 50 cm 3 et <= 125 cm 3                                                                                                                                                                                                                          '),
(32,	'Scooter > 50 cm 3 et <= 125 cm 3                                                                                                                                                                                                                               '),
(33,	'Motocyclette > 125 cm 3                                                                                                                                                                                                                                        '),
(34,	'Scooter > 125 cm 3                                                                                                                                                                                                                                             '),
(35,	'Quad léger <= 50 cm 3 (Quadricycle à moteur non carrossé)                                                                                                                                                                                                      '),
(36,	'Quad lourd > 50 cm 3 (Quadricycle à moteur non carrossé)                                                                                                                                                                                                       '),
(37,	'Autobus                                                                                                                                                                                                                                                        '),
(38,	'Autocar                                                                                                                                                                                                                                                        '),
(39,	'Train                                                                                                                                                                                                                                                          '),
(40,	'Tramway                                                                                                                                                                                                                                                        '),
(99,	'Autre véhicule                                                                                                                                                                                                                                                 ');

DROP TABLE IF EXISTS "choc";
CREATE TABLE "public"."choc" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Choc_id_choc" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "choc" ("id", "description") VALUES
(-1,	'Non renseigné                                                                                                                                                                                                                                                  '),
(0,	'Aucun                                                                                                                                                                                                                                                          '),
(1,	'Avant                                                                                                                                                                                                                                                          '),
(2,	'Avant droit                                                                                                                                                                                                                                                    '),
(3,	'Avant gauche                                                                                                                                                                                                                                                   '),
(4,	'Arrière                                                                                                                                                                                                                                                        '),
(5,	'Arrière droit                                                                                                                                                                                                                                                  '),
(6,	'Arrière gauche                                                                                                                                                                                                                                                 '),
(7,	'Côté droit                                                                                                                                                                                                                                                     '),
(8,	'Côté gauche                                                                                                                                                                                                                                                    '),
(9,	'Chocs multiples (tonneaux)                                                                                                                                                                                                                                     ');

DROP TABLE IF EXISTS "collision";
CREATE TABLE "public"."collision" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Collision_id_collision" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "collision" ("id", "description") VALUES
(-1,	'Non renseigné                                                                                                                                                                                                                                                  '),
(1,	'Deux véhicules - frontale                                                                                                                                                                                                                                      '),
(2,	'Deux véhicules - par l''arrière                                                                                                                                                                                                                                 '),
(3,	'Deux véhicules – par le coté                                                                                                                                                                                                                                   '),
(4,	'Trois véhicules et plus – en chaîne                                                                                                                                                                                                                            '),
(5,	'Trois véhicules et plus - collisions multiples                                                                                                                                                                                                                 '),
(6,	'Autre collision                                                                                                                                                                                                                                                '),
(7,	'Sans collision                                                                                                                                                                                                                                                 ');

DROP TABLE IF EXISTS "gravite";
CREATE TABLE "public"."gravite" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Gravite_id_grav" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "gravite" ("id", "description") VALUES
(1,	'Indemne                                                                                                                                                                                                                                                        '),
(2,	'Tué                                                                                                                                                                                                                                                            '),
(3,	'Blessé hospitalisé                                                                                                                                                                                                                                             '),
(4,	'Blessé léger                                                                                                                                                                                                                                                   ');

DROP TABLE IF EXISTS "intersection";
CREATE TABLE "public"."intersection" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Intersection_id_int" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "intersection" ("id", "description") VALUES
(1,	'Hors intersection                                                                                                                                                                                                                                              '),
(2,	'Intersection en X                                                                                                                                                                                                                                              '),
(3,	'Intersection en T                                                                                                                                                                                                                                              '),
(4,	'Intersection en Y                                                                                                                                                                                                                                              '),
(5,	'Intersection à plus de 4 branches                                                                                                                                                                                                                              '),
(6,	'Giratoire                                                                                                                                                                                                                                                      '),
(7,	'Place                                                                                                                                                                                                                                                          '),
(8,	'Passage à niveau                                                                                                                                                                                                                                               '),
(9,	'Autre intersection                                                                                                                                                                                                                                             ');

DROP TABLE IF EXISTS "lumiere";
CREATE TABLE "public"."lumiere" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Lumiere_id_lum" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "lumiere" ("id", "description") VALUES
(1,	'Plein jour                                                                                                                                                                                                                                                     '),
(2,	'Crépuscule ou aube                                                                                                                                                                                                                                             '),
(3,	'Nuit sans éclairage public                                                                                                                                                                                                                                     '),
(4,	'Nuit avec éclairage public non allumé                                                                                                                                                                                                                          '),
(5,	'Nuit avec éclairage public allumé                                                                                                                                                                                                                              ');

DROP TABLE IF EXISTS "num_veh";
CREATE TABLE "public"."num_veh" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "NumVech_id_numVech" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "num_veh" ("id", "description") VALUES
(1,	'Véhicule en stationnement                                                                                                                                                                                                                                      '),
(2,	'Arbre                                                                                                                                                                                                                                                          '),
(3,	'Glissière métallique                                                                                                                                                                                                                                           '),
(4,	'Glissière béton                                                                                                                                                                                                                                                '),
(5,	'Autre glissière                                                                                                                                                                                                                                                '),
(6,	'Bâtiment, mur, pile de pont                                                                                                                                                                                                                                    '),
(7,	'Support de signalisation verticale ou poste d’appel d’urgence                                                                                                                                                                                                  '),
(8,	'Poteau                                                                                                                                                                                                                                                         '),
(9,	'Mobilier urbain                                                                                                                                                                                                                                                '),
(10,	'Parapet                                                                                                                                                                                                                                                        '),
(11,	'Ilot, refuge, borne haute                                                                                                                                                                                                                                      '),
(12,	'Bordure de trottoir                                                                                                                                                                                                                                            '),
(13,	'Fossé, talus, paroi rocheuse                                                                                                                                                                                                                                   '),
(14,	'Autre obstacle fixe sur chaussée                                                                                                                                                                                                                               '),
(15,	'Autre obstacle fixe sur trottoir ou accotement                                                                                                                                                                                                                 '),
(16,	'Sortie de chaussée sans obstacle                                                                                                                                                                                                                               ');

DROP TABLE IF EXISTS "obsm";
CREATE TABLE "public"."obsm" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "obsm_id_omh" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "obsm" ("id", "description") VALUES
(0,	'Aucun                                                                                                                                                                                                                                                          '),
(1,	'Piéton                                                                                                                                                                                                                                                         '),
(2,	'Véhicule                                                                                                                                                                                                                                                       '),
(4,	'Véhicule sur rail                                                                                                                                                                                                                                              '),
(5,	'Animal domestique                                                                                                                                                                                                                                              '),
(6,	'Animal sauvage                                                                                                                                                                                                                                                 '),
(9,	'Autre                                                                                                                                                                                                                                                          ');

DROP TABLE IF EXISTS "sexe";
DROP SEQUENCE IF EXISTS sexe_id_sexe_seq;
CREATE SEQUENCE sexe_id_sexe_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 START  CACHE 1;

CREATE TABLE "public"."sexe" (
    "id" integer DEFAULT nextval('sexe_id_sexe_seq') NOT NULL,
    "description" text NOT NULL,
    CONSTRAINT "sexe_id_sexe" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "sexe" ("id", "description") VALUES
(1,	'Homme'),
(2,	'Femme');

DROP TABLE IF EXISTS "surface";
CREATE TABLE "public"."surface" (
    "id" smallint NOT NULL,
    "description" character(255) NOT NULL,
    CONSTRAINT "Surface_id_surf" PRIMARY KEY ("id")
) WITH (oids = false);

INSERT INTO "surface" ("id", "description") VALUES
(-1,	'Non renseigné                                                                                                                                                                                                                                                  '),
(1,	'Normale                                                                                                                                                                                                                                                        '),
(2,	'Mouillée                                                                                                                                                                                                                                                       '),
(3,	'Flaques                                                                                                                                                                                                                                                        '),
(4,	'Inondée                                                                                                                                                                                                                                                        '),
(5,	'Enneigée                                                                                                                                                                                                                                                       '),
(6,	'Boue                                                                                                                                                                                                                                                           '),
(7,	'Verglacée                                                                                                                                                                                                                                                      '),
(8,	'Corps gras - huile                                                                                                                                                                                                                                             '),
(9,	'Autre                                                                                                                                                                                                                                                          ');

-- 2020-11-23 10:04:50.368417+00
