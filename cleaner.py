import os, math
import pandas as pd
import numpy as np
from datetime import datetime

directory = os.path.dirname(os.path.abspath(__file__))
years = [2016,2017]
file_names=["caracteristiques","vehicules","usagers","lieux"]

result = {}
def clean_caract(csv_path_raw):
    # Read accidents csv
        df = pd.read_csv(
            csv_path_raw,
            encoding='latin-1'
        )

        # Drop columns which will not be used
        gps_to_drop = [
            "gps",
            "adr"
        ]

        df.drop(columns=gps_to_drop, inplace=True)

        # Date handling
        dates = []
        for i in df.index:
            year = int("20" + str(df["an"][i]))
            hrmn = (4 - len(str(df["hrmn"][i]))) * "0" + str(df["hrmn"][i])
            hours = int(hrmn[:2])
            mins = int(hrmn[2:])
            dt = datetime(year, df["mois"][i], df["jour"][i], hours, mins)
            formated_date = dt.isoformat(timespec='minutes')
            dates.append(formated_date)

        df["date"] = dates

        date_to_drop = [
            "an",
            "mois",
            "jour",
            "hrmn"
        ]

        df.drop(columns=date_to_drop, inplace=True)

        # Check all fields with errors
        atms = []
        cols = []
        lats = []
        longs = []
        ints = []
        
        for i in df.index:

            # atm errors handling
            atm = df["atm"][i]
            if pd.isnull(atm):
                atm = -1
            elif type(atm) is np.float64:
                atm = np.intc(atm)

            atms.append(np.int64(atm))
            
            # col errors handling
            col = df["col"][i]
            if pd.isnull(col):
                col = -1
            elif type(col) is np.float64:
                col = np.intc(col)
            cols.append(np.int64(col))

            lat = df["lat"][i]
            if(pd.isnull(lat)):
                lat = 0
            lats.append(np.int32(lat))

            lon = df["long"][i]
            if(pd.isnull(lon)):
                lon = 0
            longs.append(np.int32(lon))

            inter = df["int"][i]
            if inter< 1 or inter>9:
                inter = 9
            ints.append(np.int32(inter))

        df["atm"] = atms
        df["col"] = cols
        df["lat"] = lats
        df["long"] = longs
        df["int"] = ints

        return df


def clean_lieux(csv_path_raw):
    # Read accidents csv
    df = pd.read_csv(
        csv_path_raw,
        encoding='latin-1',
        low_memory=False
    )

    # Drop columns which will not be used
    fields_to_drop = [
        "voie",
        "v1",
        "v2",
        "circ",
        "nbv",
        "pr",
        "pr1",
        "vosp",
        "prof",
        "plan",
        "lartpc",
        "larrout",
        "env1",
        "infra",
        "situ"
    ]

    df.drop(columns=fields_to_drop, inplace=True)

    # Check all fields with errors
    catrs = []
    surfs = []
    for i in df.index:
        # catr errors handling
        catr = df["catr"][i]
        if math.isnan(catr):
            catr = 0
        elif type(catr) is np.int64:
            pass
        elif type(catr) is np.float64:
            catr = np.intc(catr)
        elif catr > 6 and catr < 9:
            catr = 0
        else:
            catr = 0
        catrs.append(catr)
        
        # surf errors handling
        surf = df["surf"][i]
        if math.isnan(surf) or surf == 0:
            surf = -1
        elif type(surf) is np.float64:
            surf = np.intc(surf)
        surfs.append(np.int32(surf))

    df["catr"] = catrs
    df["surf"] = surfs

    return df


def clean_usagers(csv_path_raw):
    # Read accidents csv
    df = pd.read_csv(
        csv_path_raw,
        encoding='latin-1',
        low_memory=False
    )

    # Drop columns which will not be used
    fields_to_drop = [
        "place",
        "trajet",
        "secu",
        "locp",
        "actp",
        "etatp"
    ]

    df.drop(columns=fields_to_drop, inplace=True)

    # Check all fields with errors and add unique identifier
    naissances = []

    for i in df.index:

        # naissance errors handling
        naissance = df["an_nais"][i]
        if pd.isnull(naissance):
            naissance = 0

        naissances.append(np.int64(naissance))

    df["an_nais"] = naissances

    return df.rename(columns={ "an_nais": "annee_naissance"})


def clean_vehicules(csv_path_raw):
    # Read accidents csv
    df = pd.read_csv(
        csv_path_raw,
        encoding='latin-1',
        low_memory=False
    )

    # Drop columns which will not be used
    fields_to_drop = [
        "senc",
        "manv",
        "obs",
        "occutc"
    ]

    df.drop(columns=fields_to_drop, inplace=True)

    # Check all fields with errors and add unique identifier

    catvs = []
    obsms = []
    chocs = []
    for i in df.index:

        # catv errors handling
        catv = df["catv"][i]
        catvs.append(np.int8(catv))

        # obsm errors handling
        obsm = df["obsm"][i]
        if math.isnan(obsm):
            obsm = 0
        elif type(obsm) is np.int64:
            pass
        elif type(obsm) is np.float64:
            obsm = np.intc(obsm)
        else:
            obs = 0
        obsms.append(obsm)


        choc = df["choc"][i]
        if pd.isnull(choc):
            choc = np.int8(-1)
        else:
            choc = np.int8(choc)
        chocs.append(choc)


    df["catv"] = catvs
    df["obsm"] = obsms
    df["choc"] = chocs

    return df

csvs = {}
for year in years :
    for index, name in enumerate(file_names):
        raw = f"{directory}/data/raw/{name}-{year}.csv"
        res = None

        if index == 0:
            res = clean_caract(raw)
        elif index == 1:
            res = clean_vehicules(raw)
        elif index == 2:
            res = clean_usagers(raw)
        elif index == 3:
            res = clean_lieux(raw)

        csvs[name] = res
    result[str(year)]=csvs

for name in file_names:
    path = f"{directory}/data/{name}.csv"
    csvs = []
    for k,v in result.items() :
        csvs.append(v[name])
    df_merged   = pd.concat(csvs, ignore_index=True)
    df_merged.to_csv(path)
